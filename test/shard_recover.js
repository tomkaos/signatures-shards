var expect = require('chai').expect;
var lib = undefined ;
lib = require('../src/index');
var BN = require('bignumber.js')
var SSSS = require('../src/sss/ssss');
var hexExamples = require('../data/randomHexes.js');

describe('shard splitting', function () {
    this.timeout(0);
    it('should reconstruct all 4 shards from shard 0 and some other shard', function () {
        this.timeout(0);

        for(var i=0;i<10/*hexExamples.length*/;i++){

            var secretIn = hexExamples[i].substr(2);
            var keys = lib.generateShards();

            for(var k=1;k<4;k++){
                console.log("iteration "+i+" second shard "+k);
                var reconstructed = lib.recoverShards(keys[0],keys[k]);
                for(var j=0;j<reconstructed.length;j++){
                    expect(reconstructed[j]).to.be.equal(keys[j]);
                    if(reconstructed[j]!=keys[j]){
                        console.log("error ",reconstructed[j],keys[j]);
                    }
                }
            }
        }
    });
});
var expect = require('chai').expect;
var lib = require('../src/index');
var BN = require('bignumber.js')
var SSSS = require('../src/sss/ssss');
var hexExamples = require('../data/randomHexes.js');

describe('project setup', function () {
  it('SSSS should work correctly',function(){
    var foo = new SSSS(2, 4, true);

    var zero = new BN("0");
    for(var i=0;i<hexExamples.length;i++){
      var a = new BN(hexExamples[i]);
      var b = foo.field_add(a,zero);
      expect(a.toString()).to.be.equal(b.toString());
    }
  });
});
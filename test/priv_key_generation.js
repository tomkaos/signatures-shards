var expect = require('chai').expect;
var lib = undefined ;
lib = require('../src/index');
var BN = require('bignumber.js')

describe('key generator', function () {
    this.timeout(0);
    it('should generate 4 shards', function () {
        var shards  = lib.generateShards();
        expect(shards.length).to.be.equal(4);
    });
    it('should generate shards that recover consistent private key',function () {
        var shards  = lib.generateShards();
        var priv = lib.getPrivKeyFromShards(shards[0],shards[1]);
        var priv2 = lib.getPrivKeyFromShards(shards[0],shards[2]);
        var priv3 = lib.getPrivKeyFromShards(shards[0],shards[3]);
        var priv4 = lib.getPrivKeyFromShards(shards[1],shards[2]);
        var priv5 = lib.getPrivKeyFromShards(shards[1],shards[3]);
        var priv6 = lib.getPrivKeyFromShards(shards[2],shards[3]);
        console.log(priv);
        expect(priv).to.be.equal(priv2);
        expect(priv).to.be.equal(priv3);
        expect(priv).to.be.equal(priv4);
        expect(priv).to.be.equal(priv5);
        expect(priv).to.be.equal(priv6);
    })
    it('should generate shards that recover consistent public key',function () {
        var shards  = lib.generateShards();
        var pub = lib.getPublicKeyFromShards(shards[0],shards[1]);
        var pub2 = lib.getPublicKeyFromShards(shards[0],shards[2]);
        var pub3 = lib.getPublicKeyFromShards(shards[0],shards[3]);
        var pub4 = lib.getPublicKeyFromShards(shards[1],shards[2]);
        var pub5 = lib.getPublicKeyFromShards(shards[1],shards[3]);
        var pub6 = lib.getPublicKeyFromShards(shards[2],shards[3]);
        console.log(pub);
        expect(pub).to.be.equal(pub2);
        expect(pub).to.be.equal(pub3);
        expect(pub).to.be.equal(pub4);
        expect(pub).to.be.equal(pub5);
        expect(pub).to.be.equal(pub6);
    });
    
});
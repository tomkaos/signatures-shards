var expect = require('chai').expect;
var lib = undefined ;
lib = require('../src/index');
var BN = require('bignumber.js')
var SSSS = require('../src/sss/ssss');
var hexExamples = require('../data/randomHexes.js');

describe('sign message', function () {
  it('shoild work if hash starts from 0x',function(){
    var hash = "0xa239110dfdb144341163f91bf4e3c9ede35ae6830a97842dfbde944eafce83ec";
    var shards = lib.generateShards();
    var signature = lib.signMessage(hash,shards[1],shards[2]);
    console.log("signature",signature);

  });
  it('should work if hash do not start from 0x',function(){
    var hash = "a239110dfdb144341163f91bf4e3c9ede35ae6830a97842dfbde944eafce83ec";
    var shards = lib.generateShards();
    var signature = lib.signMessage(hash,shards[1],shards[2]);
    console.log("signature",signature);
  });
  it('shoild fail if message is not a hash',function(){
    var notHash = "a239110dfdb144341163f91bf4e3c9ede35ae6830a97842dfbde944eafce83ec1234"
    var shards = lib.generateShards();
    var failed = true;
    try{
      var signature = lib.signMessage(notHash,shards[1],shards[2]);
      failed = false;
    }catch{}
    expect(failed).to.be.equal(true);
  });
  it('shoild fail if message is not hex',function(){
    var notHash = "z239110dfdb144341163f91bf4e3c9ede35ae6830a97842dfbde944eafce83ec"
    var shards = lib.generateShards();
    var failed = true;
    try{
      var signature = lib.signMessage(notHash,shards[1],shards[2]);
      failed = false;
    }catch{}
    expect(failed).to.be.equal(true);
  });
  it('shoild recover correct public key',function(){
    
  });
});
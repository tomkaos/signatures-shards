
var SSSS = require('./sss/ssss.js');
const eth = require('ethereumjs-wallet');
const ethUtil= require('ethereumjs-util');


  var generateShards = function() {
    var privKey = undefined;
    do{
      var ethInstance = eth.generate();
      privKey = ethInstance.getPrivateKey().toString('hex');
      var pubKey = ethInstance.getAddress().toString('hex');
    }while(privKey.substr(2).indexOf('0')==-1)

    var threshold = 2
    var numKeys = 4
    var inputIsHex = true;
    var shardsFactory = new SSSS(threshold, numKeys, inputIsHex);
    var shards = shardsFactory.split(privKey,"C4");
    return shards;
  }

  var signMessage = function(msgHash,shard1,shard2){

    var threshold = 2
    var numKeys = 4
    var inputIsHex = true;
    var shardsFactory = new SSSS(threshold, numKeys, inputIsHex);
    var privateKey = shardsFactory.combine([shard1,shard2]);
    var privKeyBuff = Buffer.from(privateKey, 'hex');
    if(msgHash.startsWith("0x")){
      msgHash = msgHash.substr(2);
    }
    var msgHash = Buffer.from(msgHash, 'hex')
    var sig = ethUtil.ecsign(msgHash, privKeyBuff)

    var serialized = ethUtil.toRpcSig(sig.v, sig.r, sig.s);

    return serialized

  }

  var validateMessage = function(msg,signature,shard1,shard2){

  }

  var getPublicKeyFromShards = function(shard1,shard2){

    var threshold = 2
    var numKeys = 4
    var inputIsHex = true;
    var shardsFactory = new SSSS(threshold, numKeys, inputIsHex);
    var secretOut = shardsFactory.combine([shard1,shard2]);
    var ethInstance = eth.fromPrivateKey(Buffer.from(secretOut, 'hex'));
    var pubKey =  ethInstance.getAddress().toString('hex');
    return pubKey;
  }

  var getPrivKeyFromShards = function(shard1,shard2){
    var threshold = 2
    var numKeys = 4
    var inputIsHex = true;
    var shardsFactory = new SSSS(threshold, numKeys, inputIsHex);
    var secretOut = shardsFactory.combine([shard1,shard2]);
    var ethInstance = eth.fromPrivateKey(Buffer.from(secretOut, 'hex'));
    var privKey = ethInstance.getPrivateKey().toString('hex');
    return privKey;
  }

  var recoverShards = function(shard_zero,shard_b){
    var threshold = 2
    var numKeys = 4
    var inputIsHex = true

    var shardsFactory = new SSSS(threshold, numKeys, inputIsHex);
    var reconstructed = shardsFactory.reconstruct_from_2_of_n("C4",shard_zero,shard_b);
    return reconstructed;
  }


  module.exports = {
      generateShards:generateShards,
      recoverShards:recoverShards,
      signMessage:signMessage,
      validateMessage:validateMessage,
      getPrivKeyFromShards:getPrivKeyFromShards,
      getPublicKeyFromShards:getPublicKeyFromShards
  }
